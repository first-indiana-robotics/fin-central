class CreatePrograms < ActiveRecord::Migration[7.0]
  def change
    create_table :programs do |t|
      t.string :name
      t.string :color
      t.string :slug

      t.timestamps
    end
    add_index :programs, :slug, unique: true
  end
end
