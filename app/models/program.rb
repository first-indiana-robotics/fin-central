class Program < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :year
end
