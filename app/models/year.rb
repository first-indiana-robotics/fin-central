class Year < ApplicationRecord
  has_many :programs

  def to_param
    return nil unless persisted?
    self.year.to_s
  end
end
