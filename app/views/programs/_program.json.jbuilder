json.extract! program, :id, :name, :color, :slug, :created_at, :updated_at
json.url program_url(program, format: :json)
