class ApplicationController < ActionController::Base
  before_action :check_for_user

  def current_user
    @current_user ||= if session[:user_id]
      User.find(session[:user_id])
    else
      nil
    end
  end

  def super_admin?
    current_user && current_user.super_admin
  end

  def check_for_user
    redirect_to root_path, error: 'Please log in first.' unless current_user
  end

  def check_for_super_admin
    redirect_to root_path, error: "You can't go there." unless super_admin?
  end

  helper_method :current_user, :super_admin?
end
