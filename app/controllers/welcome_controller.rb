class WelcomeController < ApplicationController
  skip_before_action :check_for_user
  
  def index
    if current_user
      render :dashboard
    else
      render :index
    end
  end
end
