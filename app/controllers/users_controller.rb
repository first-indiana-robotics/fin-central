class UsersController < ApplicationController
  def index
    check_for_super_admin
    
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end
end
