module ProgramHelper
  def program_badge(program)
    classes = 'badge rounded-pill program-badge'
    style = "background-color: #{program.color}; color: #{contrasting_color program.color}; text-decoration: none"

    content_tag :span, program.name, class: classes, style: style
  end
end