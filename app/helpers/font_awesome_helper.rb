module FontAwesomeHelper
  def font_awesome_kit_url
    ENV['FONT_AWESOME_URL'] || 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js'
  end

  def icon(icon, variant="s", options={})
    opts = {
      variant: 's'
    }.merge(options).with_indifferent_access
    content_tag(:i, '', class: [
      'fa-fw',
      "fa#{opts['variant']}",
      "fa-#{icon}",
      *options
      ])
  end
end