module YearHelper
  def year_badge(y)
    classes = "badge rounded-pill year-badge"
    style = "background-color: #9a989a; color: black;"

    content_tag :span, "20#{y.year - 1} - #{y.year}", class: classes, style: style
  end
end