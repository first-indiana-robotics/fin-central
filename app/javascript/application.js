// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
import "./controllers"
import './modules/offcanvas'
import './modules/datatables'
import * as bootstrap from "bootstrap"
