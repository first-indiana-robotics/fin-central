Rails.application.routes.draw do
  resources :years
  resources :programs

  post '/auth/:provider/callback' => 'sessions#create'
  get '/auth/destroy' => 'sessions#destroy', as: 'destroy_session'

  resources :users, only: %w{index show}
  root to: 'welcome#index'
end
