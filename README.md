# FIN Central

Like the FIRST HQ dashboard, but just for Indiana. And better.

## What Does This Do?

Right now, not much. But eventually, we want it to handle...

* FIN C&R form management
* FLL & FTC event registration
* Team finance pieces (event registration fee management & that kind of stuff)

... and more!

## Getting Started

Install things correctly
* Clone the project, install with yarn and bundler, and all that jazz
    * Rails 7
    * Node and yarn are still used

We use Docker to run the database and mailing service
* PostgreSQL Database
* MailCatcher
* Run `docker-compose up` to start it

Make sure that you run it correctly!
* Use `./bin/dev` to run the project instead of `rails s`

Find an issue tag to work on, make an appropriate branch for it, and then work on it
* It'll look something along the lines of `[number]-[name-seperated-by-hyphens]`
* When you're ready to push it, do so. And then create a merge request starting with the number of the issue.

## Requests for Developers

* Sequentially number new database migrations
* New database tables should to use UUIDs for their IDs
* [FriendlyID](https://github.com/norman/friendly_id) is a thing, please use it whenever possible